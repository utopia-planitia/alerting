apiVersion: v1
kind: Secret
metadata:
  name: client-configuration
type: Opaque
stringData:
{{ (tpl (.Files.Glob "secrets/client-configuration/*").AsConfig . ) | indent 2 }}
